module "servers" {
  source                 = "./module/waf"
  waf_ipset_name         = "waf_ipset_name"
  waf_ipset_value        = "10.0.0.0/24"
  waf_rule_name          = "waf_rule_name"
  waf_rule_metrics       = "metrics"
  waf_rule_group_name    = "waf_rule_group"
  web_acl_name           = "web_acl_name"
  web_acl_metrics        = "metrics"
  waf_rule_group_metrics = "metrics"
  aws_region             = "us-east-1"
}