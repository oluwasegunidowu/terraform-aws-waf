## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_waf_ipset.wafipset](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/waf_ipset) | resource |
| [aws_waf_rule.wafrule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/waf_rule) | resource |
| [aws_waf_rule_group.rule_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/waf_rule_group) | resource |
| [aws_waf_web_acl.waf_acl](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/waf_web_acl) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | n/a | `string` | `"us-east-1"` | no |
| <a name="input_waf_ipset_name"></a> [waf\_ipset\_name](#input\_waf\_ipset\_name) | n/a | `string` | `"waf_ipset_name"` | no |
| <a name="input_waf_ipset_value"></a> [waf\_ipset\_value](#input\_waf\_ipset\_value) | n/a | `string` | n/a | yes |
| <a name="input_waf_rule_group_metrics"></a> [waf\_rule\_group\_metrics](#input\_waf\_rule\_group\_metrics) | n/a | `string` | n/a | yes |
| <a name="input_waf_rule_group_name"></a> [waf\_rule\_group\_name](#input\_waf\_rule\_group\_name) | n/a | `string` | `"waf_rule_group"` | no |
| <a name="input_waf_rule_metrics"></a> [waf\_rule\_metrics](#input\_waf\_rule\_metrics) | n/a | `string` | `"metrics"` | no |
| <a name="input_waf_rule_name"></a> [waf\_rule\_name](#input\_waf\_rule\_name) | n/a | `string` | `"waf_rule_name"` | no |
| <a name="input_web_acl_metrics"></a> [web\_acl\_metrics](#input\_web\_acl\_metrics) | n/a | `string` | n/a | yes |
| <a name="input_web_acl_name"></a> [web\_acl\_name](#input\_web\_acl\_name) | n/a | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aws_waf_rule_arn"></a> [aws\_waf\_rule\_arn](#output\_aws\_waf\_rule\_arn) | n/a |
| <a name="output_aws_waf_rule_group_arn"></a> [aws\_waf\_rule\_group\_arn](#output\_aws\_waf\_rule\_group\_arn) | n/a |
| <a name="output_aws_waf_rule_group_id"></a> [aws\_waf\_rule\_group\_id](#output\_aws\_waf\_rule\_group\_id) | n/a |
| <a name="output_aws_waf_rule_id"></a> [aws\_waf\_rule\_id](#output\_aws\_waf\_rule\_id) | n/a |
| <a name="output_aws_waf_web_acl_arn"></a> [aws\_waf\_web\_acl\_arn](#output\_aws\_waf\_web\_acl\_arn) | n/a |
| <a name="output_aws_waf_web_acl_id"></a> [aws\_waf\_web\_acl\_id](#output\_aws\_waf\_web\_acl\_id) | n/a |
