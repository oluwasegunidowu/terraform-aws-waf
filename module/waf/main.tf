# this creates an IP Set to be defined in AWS WAF
resource "aws_waf_ipset" "wafipset" {
  name = var.waf_ipset_name

  ip_set_descriptors {
    type  = "IPV4"
    value = var.waf_ipset_value
  }
}

# This creats an AWS WAF rule that will be applied on AWS Web ACL
resource "aws_waf_rule" "wafrule" {
  depends_on  = [aws_waf_ipset.wafipset]
  name        = var.waf_rule_name
  metric_name = var.waf_rule_metrics

  predicates {
    data_id = aws_waf_ipset.wafipset.id
    negated = false
    type    = "IPMatch"
  }
}

# This creates Rule Group which will be applied on  AWS Web ACL
resource "aws_waf_rule_group" "rule_group" {
  name        = var.waf_rule_group_name
  metric_name = var.waf_rule_metrics

  activated_rule {
    action {
      type = "COUNT"
    }

    priority = 50
    rule_id  = aws_waf_rule.wafrule.id
  }
}

# Creating the Web ACL component in AWS WAF
resource "aws_waf_web_acl" "waf_acl" {
  depends_on = [
    aws_waf_ipset.wafipset,
    aws_waf_rule.wafrule,
  ]
  name        = var.web_acl_name
  metric_name = var.web_acl_metrics

  default_action {
    type = "ALLOW"
  }

  rules {
    action {
      type = "BLOCK"
    }

    priority = 1
    rule_id  = aws_waf_rule.wafrule.id
    type     = "REGULAR"
  }
}