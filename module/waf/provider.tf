terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  #region = var.aws_provider ##wrong region name this should have been var.aws_region
  region = var.aws_region
}