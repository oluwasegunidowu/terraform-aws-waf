variable "waf_ipset_name" {
  type    = string
  default = "waf_ipset_name"
}

variable "waf_ipset_value" {
  type = string
}

variable "waf_rule_name" {
  type    = string
  default = "waf_rule_name"
}

variable "waf_rule_metrics" {
  type    = string
  default = "metrics"
}

variable "waf_rule_group_name" {
  type    = string
  default = "waf_rule_group"
}

variable "web_acl_name" {
  type = string
}

variable "web_acl_metrics" {
  type = string
}

variable "waf_rule_group_metrics" {
  type = string
}

variable "aws_region" {
  type    = string
  default = "us-east-1"
}